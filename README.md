# lamp

#### Table of Contents

1. [Overview](#overview)
2. [Module Description](#module-description)
3. [Limitations](#limitations)

## Overview

Base module for configuring Apache 2.4.x, PHP 5.x (the current latest
available as a native package), and SimpleSAMLphp on Ubuntu Trusty.

## Module Description

## Limitations

Only tested on Ubuntu; it would probably work on Debian if it was added
to _metadata.json_.


