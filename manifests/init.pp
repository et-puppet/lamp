# configure an Emerging Technology LAMP + SimpleSAMLphp image

class lamp (
  $simplesamlphp_version,
  $php_name,
  $php_dir,
) {

  package {
    [
      "${php_name}-cli",
      "${php_name}-curl",
      "${php_name}-gd",
      "${php_name}-mcrypt",
      "${php_name}-mysql",
      'mysql-client',
    ]:
      ensure  => 'present',
      require => Package['httpd'],
  }

  # add the admin user to the www-data group
  user { 'admin':
    groups  => 'www-data',
    require => Package['httpd'],
  }

  package { 'simplesamlphp':
    ensure  => $simplesamlphp_version,
    require => Package["${php_name}-mcrypt"],
  }

  class { 'apache':
    service_enable      => false,
    service_ensure      => 'stopped',
    default_mods        => false,
    default_confd_files => false,
    default_vhost       => false,
    log_formats         => {
      vhost_common => '%v %h %l %u %t \"%r\" %>s %b',
      combined_elb => '%v:%p %{X-Forwarded-For}i %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"'
    },
    #logroot             => '/var/log/apache2',
    logroot             => '/dev',
    mpm_module          => 'prefork',
  }

  apache::vhost { 'lamp':
    port                => 8080,
    docroot             => '/var/www',
    docroot_owner       => 'root',
    docroot_group       => 'www-data',
    docroot_mode        => '0755',
    servername          => '${ENV_DOMAIN}',
    serveradmin         => 'support@anchorage.stanford.edu',
    access_log_format   => 'combined_elb',
    access_log_file     => 'stdout',
    error_log_file      => 'stdout',
    directories         => [
      {
        path           => '/var/www',
        allow_override => ['All'],
      },
      {
        path         => '/usr/share/simplesamlphp/www',
        auth_require => 'all granted',
      },
    ],
    aliases             => [
      {
        alias => '/simplesamlphp',
        path  => '/usr/share/simplesamlphp/www',
      }
    ],
    additional_includes => [
      '/etc/apache2/mod_file_cache.conf',
    ],
  }

  class {'::apache::mod::php':
    notify => File['php-anchorage-ini'],
  }

  apache::mod {
    [
      'env',
      'rewrite',
      'authn_core',
      'access_compat',
      'proxy',
      'proxy_http',
      'file_cache',
      'expires',
      'headers',
    ]:
  }

  file {
    [
      '/var/log/apache2',
      '/var/lock/apache2',
      '/var/run/apache2',
    ]:

    ensure  => directory,
    owner   => 'www-data',
    group   => 'www-data',
    mode    => '0755',
    require => Package['httpd'],
  }


  file { '/etc/apache2/conf.d/platform_env.conf':
    ensure => file,
    owner  => '0',
    group  => '0',
    mode   => '0644',
    source => 'puppet:///modules/lamp/apache2/platform_env.conf',
  }

  file { '/etc/simplesamlphp/certs':
    ensure  => directory,
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    require => Package['simplesamlphp'],
  }

  file { '/etc/simplesamlphp/config.php':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules/lamp/simplesamlphp/config.php',
    require => Package['simplesamlphp'],
  }

  file { '/etc/simplesamlphp/config.local.php':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    require => Package['simplesamlphp'],
  }

  file { '/etc/simplesamlphp/authsources.php':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules/lamp/simplesamlphp/authsources.php',
    require => Package['simplesamlphp'],
  }

  file { '/etc/simplesamlphp/metadata/saml20-idp-remote.php':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules/lamp/simplesamlphp/saml20-idp-remote.php',
    require => Package['simplesamlphp'],
  }

  file { '/opt/simplesamlphp':
    ensure => link,
    target => '/usr/share/simplesamlphp',
  }

  file { 'php-anchorage-ini':
    ensure  => file,
    path    => "${php_dir}/mods-available/anchorage.ini",
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules/lamp/php/anchorage.ini',
  }

  file { '/etc/apache2/mod_file_cache.conf':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => '',
  }

  file { '/start.sh':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => 'puppet:///modules/lamp/start.sh',
  }

  exec { 'enable-php-modules':
    path    => '/bin/:/usr/bin:/usr/sbin',
    command => "${php_name}enmod mcrypt anchorage",
    require => [
      File["${php_dir}/mods-available/anchorage.ini"],
      Package["${php_name}-mcrypt"],
    ],
  }

}

