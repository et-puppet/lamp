#! /bin/bash

rm -f /tmp/.newrelic.sock
rm -rf /var/log/newrelic/*

case $1 in
  ssh)
    mkdir -p -m0755 /var/run/sshd
    exec /usr/sbin/sshd -D -o PermitUserEnvironment=yes
    ;;
  *)
    # for mod_file_cache on drupal
    ( cd /var/www/sites/all; find modules themes -name '*.js' -o -name '*.css' |\
      sed 's/^/MMapFile /' ) >> /etc/apache2/mod_file_cache.conf
    exec /usr/sbin/apache2ctl -D FOREGROUND -k start
    ;;
esac

