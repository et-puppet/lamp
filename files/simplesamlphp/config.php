<?php

// fix SERVER vars behind reverse proxy
if (array_key_exists('HTTP_X_FORWARDED_PROTO', $_SERVER) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
   $_SERVER['HTTPS'] = 'on';
   $_SERVER['SERVER_PORT'] = 443;
}

$config = array (
  'baseurlpath'                 => rtrim($_SERVER['ENV_URL'],'/') . '/simplesamlphp/',
  'certdir'                     => '/etc/simplesamlphp/certs/',
  'metadatadir'                 => '/etc/simplesamlphp/metadata',
  'attributenamemapdir'         => '/etc/simplesamlphp/attributemap',
  'loggingdir'                  => '/var/log/simplesamlphp/',
  'datadir'                     => '/var/lib/simplesamlphp/data/',
  'debug'                       => FALSE,
  'debug.validatexml'           => FALSE,
  'technicalcontact_name'       => 'Administrator',
  'technicalcontact_email'      => $_SERVER['ADMIN_MAIL'],
  'timezone'                    => 'America/Los_Angeles',
  'logging.processname'         => 'simplesamlphp',
  'session.duration'            =>  8 * (60*60), // 8 hours.
  'session.datastore.timeout'   => (4*60*60), // 4 hours
  'session.state.timeout'       => (60*60), // 1 hour
  'session.cookie.secure'       => FALSE,
  'session.phpsession.httponly' => FALSE,
  'language.default'            => 'en',

  /*
   * Authentication processing filters that will be executed for all SPs
   * Both Shibboleth and SAML 2.0
   */
  'authproc.sp' => array(
    10 => array(
      'class'                 => 'core:AttributeMap',
      'oid2name',
    ),
    /*
     * convert : and / in group names to _
     */
    50 => array(
      'class'       => 'core:AttributeAlter',
      'subject'     => 'eduPersonEntitlement',
      'pattern'     => '/[\/\:]/',
      'replacement' => '_',
    ),

    90 => 'core:LanguageAdaptor',
  ),

  'metadata.sources' => array(
    array('type' => 'flatfile'),
  ),

  'store.type'          => 'sql',
  'store.sql.dsn'       => 'mysql:host=' . $_SERVER['RDS_HOSTNAME']
                         . ';dbname=' . $_SERVER['RDS_DB_NAME'],
  'store.sql.username'  => $_SERVER['RDS_USERNAME'],
  'store.sql.password'  => $_SERVER['RDS_PASSWORD'],
  'store.sql.prefix'    => 'simpleSAMLphp'
);

require_once('/var/lib/simplesamlphp/secrets.inc.php');
require_once('/etc/simplesamlphp/config.local.php');
