<?php

$config = array(
  'admin' => array(
    'core:AdminPassword',
  ),

  'default-sp' => array(
    'saml:SP',

    'authproc' => array(
      20 => 'saml:NameIDAttribute',
    ),

    'entityID' => $_SERVER['ENV_ENTITY_ID'],
    'idp' => $_SERVER['ENV_IDP'],

  ),
);

