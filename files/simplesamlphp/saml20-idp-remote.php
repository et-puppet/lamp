<?php

$metadata['https://weblogin.itlab.stanford.edu/idp/shibboleth'] = array(
  'name' => array(
    'en' => 'Stanford IT Lab IdP',
  ),
  'description'         => 'Stanford IT Lab IdP',
  'SingleSignOnService' => 'https://weblogin.itlab.stanford.edu/idp/profile/SAML2/Redirect/SSO',
  'certFingerprint'     => '87:5E:8B:F0:ED:DD:5F:6D:DA:E3:67:AE:F9:11:67:A2:5E:BE:39:CC'
);

$metadata['https://idpproxy.itlab.stanford.edu/idp'] = array (
  'name' => array(
    'en' => 'Stanford IT Lab IdP Proxy',
  ),
  'description'         => 'Stanford IT Lab IdP Proxy',
  'SingleSignOnService' => 'https://idpproxy.itlab.stanford.edu/simplesamlphp/saml2/idp/SSOService.php',
  'certFingerprint'     => 'DD:04:2D:D3:AF:AC:E8:CB:70:95:18:DF:B5:A3:88:73:1F:F1:6C:3F',
);

$metadata['https://idpproxy.anchorage.stanford.edu/idp'] = array (
  'name' => array(
    'en' => 'Stanford Anchorage IdP Proxy',
  ),
  'description'         => 'Stanford Anchorage IdP Proxy',
  'SingleSignOnService' => 'https://idpproxy.anchorage.stanford.edu/simplesamlphp/saml2/idp/SSOService.php',
  'certFingerprint'     => '6D:B3:A5:8D:36:01:30:C1:46:86:07:EC:8C:8F:19:D5:CB:57:7D:D9',
);

$metadata['https://idp-dev.stanford.edu/'] = array(
  'name' => array(
    'en' => 'Stanford University Dev WebAuth',
  ),
  'description'         => 'Stanford University Dev WebAuth',
  'SingleSignOnService' => 'https://idp-dev.stanford.edu/idp/profile/SAML2/Redirect/SSO',
  'certFingerprint'     => '0F:6F:50:00:54:B6:13:3B:99:17:61:FD:F3:AD:62:F7:86:D4:F1:5B'
);

$metadata['https://idp-uat.stanford.edu/'] = array(
  'name' => array(
    'en' => 'Stanford University UAT WebAuth',
  ),
  'description'         => 'Stanford University UAT WebAuth',
  'SingleSignOnService' => 'https://idp-uat.stanford.edu/idp/profile/SAML2/Redirect/SSO',
  'certFingerprint'     => '6E:C8:18:F6:F9:3D:00:9D:8D:AB:18:02:FD:1A:41:14:ED:98:E4:31'
);

$metadata['https://idp.stanford.edu/'] = array(
  'name' => array(
    'en' => 'Stanford University WebAuth',
  ),
  'description'         => 'Stanford University WebAuth',
  'SingleSignOnService' => 'https://idp.stanford.edu/idp/profile/SAML2/Redirect/SSO',
  'certFingerprint'     => '2B:41:A2:66:6A:4E:3F:40:C6:30:55:6A:1F:EC:C3:E3:0B:CE:EE:8F'
);
